var searchData=
[
  ['adagrad',['Adagrad',['../structAdagrad.html',1,'']]],
  ['adagrad_5fbeta',['Adagrad_beta',['../structParameters.html#a9fc2748adaa357bf6e4ad14bcd86ade9',1,'Parameters']]],
  ['adam',['Adam',['../structAdam.html',1,'']]],
  ['adam_5fbeta1',['Adam_beta1',['../structParameters.html#a8c6cd65deec4169ae0cdefffcb8e2252',1,'Parameters']]],
  ['adam_5fbeta2',['Adam_beta2',['../structParameters.html#a2a80a92cc9f7a7748293b1aee8eb44d9',1,'Parameters']]],
  ['adam_5fgamma',['Adam_gamma',['../structParameters.html#a16a784f47a8334f61e84429b4397f179',1,'Parameters']]]
];
