#include "ConfigActions.h"

Real Parameters::Adam_beta1;
Real Parameters::Adam_beta2;
Real Parameters::Adam_gamma;

Real Parameters::RmsProp_beta;
Real Parameters::RmsProp_alpha;

Real Parameters::Adagrad_beta;

Real Parameters::AdaMax_beta1;
Real Parameters::AdaMax_beta2;
Real Parameters::AdaMax_gamma;

Real Parameters::Adadelta_beta;

Real Parameters::Nadam_beta1;
Real Parameters::Nadam_beta2;
Real Parameters::Nadam_gamma;


void createDefaultConfigFile() {
  json config;

  config["Adam"]["beta1"] = 0.84374857;
  config["Adam"]["beta2"] = 0.89508866;
  config["Adam"]["gamma"] = 0.03470107;

  config["RmsProp"]["beta"] = 0.93005474;
  config["RmsProp"]["alpha"] = 0.063761147;

  config["AdaGrad"]["beta"] = 0.9;

  config["AdaMax"]["beta1"] = 0.92096770;
  config["AdaMax"]["beta2"] = 0.95563197;
  config["AdaMax"]["gamma"] = 0.18211471;

  config["Adadelta"]["beta"] = 0.9;

  config["Nadam"]["beta1"] = 0.88441989;
  config["Nadam"]["beta2"] = 0.88441989;
  config["Nadam"]["gamma"] = 0.08381000;

  std::ofstream configFile("config.json");
  configFile << std::setw(4) << config << std::endl;
}

void getParametersFromConfig() {
  std::ifstream configFile("config.json");
  json configData;

  configFile >> configData;

  Parameters::Adam_beta1 = configData["Adam"]["beta1"];
  Parameters::Adam_beta2 = configData["Adam"]["beta1"];
  Parameters::Adam_gamma = configData["Adam"]["gamma"];

  Parameters::RmsProp_beta = configData["RmsProp"]["beta"];
  Parameters::RmsProp_alpha = configData["RmsProp"]["alpha"];

  Parameters::Adagrad_beta = configData["AdaGrad"]["beta"];

  Parameters::AdaMax_beta1 = configData["AdaMax"]["beta1"];
  Parameters::AdaMax_beta2 = configData["AdaMax"]["beta2"];
  Parameters::AdaMax_gamma = configData["AdaMax"]["gamma"];

  Parameters::Adadelta_beta = configData["Adadelta"]["beta"];

  Parameters::Nadam_beta1 = configData["Nadam"]["beta1"];
  Parameters::Nadam_beta2 = configData["Nadam"]["beta1"];
  Parameters::Nadam_gamma = configData["Nadam"]["gamma"];
}

