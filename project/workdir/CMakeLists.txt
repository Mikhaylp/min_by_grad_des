cmake_minimum_required(VERSION 2.8.9)

project(min_by_grad_des)

include_directories("include" "include/Methods" "include/Tools")

file(GLOB SOURCES "test/*.cpp" "src/*.cpp" "src/Methods/*.cpp" "src/Tools/*.cpp")

add_subdirectory(json)

add_executable(test ${SOURCES})
