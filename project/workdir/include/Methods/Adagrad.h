#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"


/**
 *
 * @struct Adagrad
 * @brief Adaptive Gradient method
 * @var Adagrad::f
 * Минимизируемая функция
 * @var Adagrad::startPoint
 * Точка старта минимизации
 * @var Adagrad::parameters
 * Параметры метода
 * @var Adagrad::grad_accuracy
 * Точность расчета градиента в методе
 * @var Adagrad::iter_limit
 * Максимальное число итераций метода
 *
*/

IterationData Adagrad(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);
