#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"


/**
 *
 * @struct Adam
 * @brief Adaptive Moment Estimation method
 * @var Adam::f
 * Минимизируемая функция
 * @var Adam::startPoint
 * Точка старта минимизации
 * @var Adam::parameters
 * Параметры метода
 * @var Adam::grad_accuracy
 * Точность расчета градиента в методе
 * @var Adam::iter_limit
 * Максимальное число итераций метода
 *
*/


IterationData Adam(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);
