#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"


/**
 *
 * @struct RmsProp
 * @brief Root Mean Square Propagation method
 * @var RmsProp::f
 * Минимизируемая функция
 * @var RmsProp::startPoint
 * Точка старта минимизации
 * @var Adam::parameters
 * Параметры метода
 * @var Adam::grad_accuracy
 * Точность расчета градиента в методе
 * @var Adam::iter_limit
 * Максимальное число итераций метода
 * 
*/


IterationData RmsProp(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);
