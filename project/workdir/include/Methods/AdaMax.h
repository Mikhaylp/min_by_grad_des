#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"


/**
 *
 * @struct AdaMax
 * @brief Adaptive Moment Estimation method
 * @var AdaMax::f
 * �������������� �������
 * @var AdaMax::startPoint
 * ����� ������ �����������
 * @var AdaMax::parameters
 * ��������� ������
 * @var AdaMax::grad_accuracy
 * �������� ������� ��������� � ������
 * @var AdaMax::iter_limit
 * ������������ ����� �������� ������
 *
*/


IterationData AdaMax(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);
