#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"


/**
*
* @struct Adadelta
* @brief Adaptive Moment Estimation method
* @var Adadelta::f
* �������������� �������
* @var Adadelta::startPoint
* ����� ������ �����������
* @var Adadelta::parameters
* ��������� ������
* @var Adadelta::grad_accuracy
* �������� ������� ��������� � ������
* @var Adadelta::iter_limit
* ������������ ����� �������� ������
*
*/


IterationData Adadelta(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);
