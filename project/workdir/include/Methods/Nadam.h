#pragma once

/* Tools */
#include "Data.h"
#include "Math.h"

#include <math.h>

/**
 *
 * @struct Nadam
 * @brief Nesterov-accelerated Adaptive Moment Estimation (Nadam)
 * @var Nadam::f
 * Минимизируемая функция
 * @var Nadam::startPoint
 * Точка старта минимизации
 * @var Nadam::parameters
 * Параметры метода
 * @var Nadam::grad_accuracy
 * Точность расчета градиента в методе
 * @var Nadam::iter_limit
 * Максимальное число итераций метода
 *
*/


IterationData Nadam(Function f, Vector startPoint, Vector parameters, Real grad_accuracy, int iter_lim);