/** @file */
#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <nlohmann/json.hpp>

#include "Parameters.h"

using json = nlohmann::json;

/**
 * @fn createDefaultConfigFile
 * @brief Функция для создания config.json
 *
*/
void createDefaultConfigFile();

/**
 *
 * @fn getParametersFromConfig
 * @brief Функция для считывания параметров из config.json
 *
*/
void getParametersFromConfig();

